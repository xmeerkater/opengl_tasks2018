//
// Created by avk on 14.03.18.
//

#ifndef STUDENTTASKS2017_MAZEAPPLICATION_H
#define STUDENTTASKS2017_MAZEAPPLICATION_H

#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include "CreateMaze.h"
#include <memory>
#include <iostream>

#include "MazeWalkerCamera.h"


class MazeApplication : public Application {
public:
    MazeApplication();

    void createFloor();

    void createWalls();
    void createCeil();

    void handleMouseMove(double x, double y) override;
    void handleKey(int key, int scancode, int action, int mods) override;

    void makeScene() override;
    void draw() override;

private:
    void _changeCameraMover();

private:
    MeshPtr _floor;
    MeshPtr _ceil;
    MazePtr _maze;
    std::vector<MeshPtr> _walls;

    bool _drawCeil = true;

    ShaderProgramPtr _shader;
    CameraMoverPtr _anotherCamera;


};


#endif //STUDENTTASKS2017_MAZEAPPLICATION_H
